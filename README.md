# Terafab Plant Planning and Automation

This repo contains code associated with plant-level planning, scheduling, logistics, and general construction management.

## Getting Up and Running

TBD.

## Dependencies

Initial environment dependencies intended for local environment per `requirement.txt`. Future iterations will transition this over to a Docker container.

## Basic API

TBD.
